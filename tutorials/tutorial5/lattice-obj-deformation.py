#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 15:43:16 2021

@author: lcalmett
"""

import numpy as np

from pygem import FFD
from smithers import io
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
def plot(data,ffd_control,color=None):

    

    if color is None:
        color = (0, 0, 1, 0.1)
    
    fig = plt.figure(figsize=(16,10))

    verts = [data['points'][cell] for cell in data['cells']]
  
    ax = fig.add_subplot(111, projection='3d')
    faces = Poly3DCollection(verts, linewidths=1, edgecolors='k')
  
    faces.set_facecolor(color)
   
    
    ax.add_collection3d(faces)
  
    ax.set_xlim3d(-10., 20.)
    ax.set_ylim3d(-10., 20.)
    ax.set_zlim3d(-10., 20.)
    ax.set_aspect('auto','box')
    ax.scatter(*ffd_control, s=50, c='red')

    plt.show()




# attention sensibilité à la postion des points la boite doit contenir 
#l'élément de manière symétrique sinon le noeud ne sont pas influencé de manière équivalente
ffd = FFD([2, 2, 2])
ffd.box_length = [20., 20., 20.]
ffd.box_origin = [-10., -10., -10.]
ffd.rot_angle  = [0. ,0., 0.]
"""
ffd_control = ffd.control_points().T
plot(io.OBJHandler.read('init.obj'),ffd_control)
filename = f'init.obj'
plt.show()

filename = f'init.pdf'
plt.savefig(filename)
#plt.close()
"""

"""
x = np.concatenate((np.transpose(a[:,28:32]),np.transpose(a[:,40:44])))



test= np.transpose(x) 
teta = 90*np.pi/180
rotation = np.array([[np.cos(teta),-np.sin(teta),0],
                     [np.sin(teta),np.cos(teta),0],
                     [0,0,1]])
test = np.dot(rotation,test)
rot= np.transpose(test)

defo = (rot-x)/120 
"""

correction =np.array([[0.02365],
                        [0.05],
                        [0.14],
                        [0.0268],
                        [0.027],
                        [0.08],
                        [0.018],
                        [0.0825],
                        [0.05845],
                        [0.0454],
                        [0.0616],
                        [0.0879],
                        [0.0654],
                        [0.0895],
                        [0.0898],
                        [0.0697],
                        [0.05845],
                        [0.0454],
                        [0.0616],
                        [0.0879],
                        [0.0654],
                        [0.0895],
                        [0.0898],
                        [0.0697]])
"""
ffd.array_mu_x[0, 0, 0] = 1

ffd.array_mu_x[1, 0, 0] = correction[1,0]
ffd.array_mu_x[1, 1, 0] = correction[2,0]
ffd.array_mu_x[0, 1, 0] = correction[3,0]    
ffd.array_mu_x[0, 1, 1] = correction[4,0]    
ffd.array_mu_x[0, 0, 1] = correction[5,0]
ffd.array_mu_x[1, 0, 1] = correction[6,0]
ffd.array_mu_x[1, 1, 1] = correction[7,0]
ffd.array_mu_y[0, 0, 0] = correction[8,0]
ffd.array_mu_y[1, 0, 0] = correction[9,0]
ffd.array_mu_y[1, 1, 0] = correction[10,0]
ffd.array_mu_y[0, 1, 0] = correction[11,0]    
ffd.array_mu_y[0, 1, 1] = correction[12,0]    
ffd.array_mu_y[0, 0, 1] = correction[13,0]
ffd.array_mu_y[1, 0, 1] = correction[14,0]
ffd.array_mu_y[1, 1, 1] = correction[15,0]
"""
#ffd.array_mu_z[0, 0, 0] = correction[16,0]
#ffd.array_mu_z[1, 0, 0] = 10
ffd.array_mu_z[1, 1, 0] = -0.5
ffd.array_mu_z[0, 1, 0] = -0.5  
ffd.array_mu_z[0, 1, 1] = 0.5   
#ffd.array_mu_z[0, 0, 1] = correction[21,0]
#ffd.array_mu_z[1, 0, 1] = 10
ffd.array_mu_z[1, 1, 1] = 0.5


"""
#rotation du tamis

ffd.array_mu_x[2, 2, 0] = defo[0,0]
ffd.array_mu_y[2, 2, 0] = defo[0,1]

ffd.array_mu_x[2, 2, 1] = defo[1,0]
ffd.array_mu_y[2, 2, 1] = defo[1,1]

ffd.array_mu_x[2, 3, 0] = defo[2,0]
ffd.array_mu_y[2, 3, 0] = defo[2,1]

ffd.array_mu_x[2, 3, 1] = defo[3,0]
ffd.array_mu_y[2, 3, 1] = defo[3,1]

ffd.array_mu_x[3, 2, 0] = defo[4,0]
ffd.array_mu_y[3, 2, 0] = defo[4,1]

ffd.array_mu_x[3, 2, 1] = defo[5,0]
ffd.array_mu_y[3, 2, 1] = defo[5,1]

ffd.array_mu_x[3, 3, 0] = defo[6,0]
ffd.array_mu_y[3, 3, 0] = defo[6,1]

ffd.array_mu_x[3, 3, 1] = defo[7,0]
ffd.array_mu_y[3, 3, 1] = defo[7,1]
"""

#ffd.read_parameters('parameters_test_ffd_iges.prm')
obj_filename = "init.obj"


obj_content = io.OBJHandler.read(obj_filename)
obj_content['points'] = ffd(obj_content['points'])
io.OBJHandler.write('deform_cube_rot.obj', obj_content)

new_mesh = obj_content['points'] 
print(ffd)
"""
a= ffd.control_points().T
print(a)
ax = plt.figure(figsize=(8, 8)).add_subplot(111, projection='3d')
#ax.scatter(*new_mesh.T)
ax.scatter(*ffd.control_points().T, s=50, c='red')
plt.show()

"""
ffd_control = ffd.control_points().T

plot(io.OBJHandler.read('deform_cube_rot.obj'),ffd_control)

plt.show()

filename = f'deform.pdf'
plt.savefig(filename)
#plt.close()

