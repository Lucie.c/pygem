#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 10:31:23 2021

@author: lcalmett
"""


import numpy as np

from pygem import FFD
from smithers import io
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
def plot(data, color=None):

    

    if color is None:
        color = (0, 0, 1, 0.1)
    fig = plt.figure(figsize=(16,10))

    verts = [data['points'][cell] for cell in data['cells']]
    ax = fig.add_subplot(111, projection='3d')
    faces = Poly3DCollection(verts, linewidths=1, edgecolors='k')
    faces.set_facecolor(color)
    
    ax.add_collection3d(faces)
    ax.set_xlim3d(-10., 20.)
    ax.set_ylim3d(-10., 20.)
    ax.set_zlim3d(-10., 20.)
    ax.set_aspect('auto','box')

    plt.show()




# attention sensibilité à la postion des points la boite doit contenir 
#l'élément de manière symétrique sinon le noeud ne sont pas influencé de manière équivalente
ffd = FFD()
ffd.box_length = [20.,20.,20.]
ffd.box_origin = [-10.,-10.,-10.]

ffd.array_mu_x[1, :,:] += 0.6

obj_filename = "cube.obj"

obj_content = io.OBJHandler.read(obj_filename)
obj_content['points'] = ffd(obj_content['points'])
io.OBJHandler.write('deform_cube.obj', obj_content)

new_mesh = obj_content['points'] 


ax = plt.figure(figsize=(8, 8)).add_subplot(111, projection='3d')
ax.scatter(*new_mesh.T)
ax.scatter(*ffd.control_points().T, s=50, c='red')
plt.show()


plot(io.OBJHandler.read('cube.obj'))
plot(io.OBJHandler.read('deform_cube.obj'), (0, 1, 0, .1))


