#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 11:20:01 2021

@author: lcalmett
"""




import numpy as np
from smithers import io
import matplotlib.pyplot as plt

from pygem import RBF
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import imageio

import sys


def plot(data, color=None):

    

    if color is None:
        color = (0, 0, 1, 0.1)
    fig = plt.figure(figsize=(16,10))

    verts = [data['points'][cell] for cell in data['cells']]
    ax = fig.add_subplot(111, projection='3d')
    faces = Poly3DCollection(verts, linewidths=1, edgecolors='k')
    faces.set_facecolor(color)
    
    ax.add_collection3d(faces)
    ax.set_xlim3d(-10., 30.)
    ax.set_ylim3d(-10., 30.)
    ax.set_zlim3d(-10., 30.)
    ax.set_aspect('auto','box')

    plt.show()
rbf = RBF()


#parameters_file = 'parameters_idw_cube.prm'
filenames = []

#idw.read_parameters(filename=parameters_file)
controle_p_ini  = rbf.original_control_points = np.array([[-10.   ,-10.0   ,-10.0],
                         [-10.0 ,  -10.0  , 10.0],
                         [-10.0   ,10.0  , -10.0],
                         [-10.0   ,10.0  , 10.0],
                         [10.0   ,-10    , -10.0],
                         [10.0   ,-10.0  , 10.0],
                         [10.0   ,10.0  , -10],
                         [10.0  , 10.0  , 10.0]])
for nombre_iter in range(10):
    control_p_deformation = controle_p_ini + 0 
    for i in range(4,8):
        control_p_deformation[i,0] += 2
        
    rbf.deformed_control_points = control_p_deformation 
    
    #l'écrire dans un ficher texte, à la fin pour récupérer les positions?
    #b=idw.write_parameters('test')
    
    obj_filename = "init.obj"
    
    obj_content = io.OBJHandler.read(obj_filename)
    obj_content['points'] = rbf(obj_content['points'])
    io.OBJHandler.write('deform_cube.obj', obj_content)
    
    new_mesh = obj_content['points'] 
    
    
    
    
    #plot(io.OBJHandler.read('cube.obj'))
    plot(io.OBJHandler.read('deform_cube.obj'), (0, 1, 0, .1))
    controle_p_ini = control_p_deformation
    
    filename = f'{nombre_iter}.png'
    filenames.append(filename)
    filenames.append(filename)
    filenames.append(filename)
    
    # save frame
    plt.savefig(filename)
    plt.close()
    

with imageio.get_writer('mygif.gif', mode='I') as writer:
    for filename in filenames:
        image = imageio.imread(filename)
        writer.append_data(image)
