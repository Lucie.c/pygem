#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 16:59:47 2021

@author: lcalmett
"""

#!/usr/bin/env python
# coding: utf-8

# # PyGeM
# 
# ## Tutorial 5: Deformation of an object stored into file
# 
# In the present tutorial, we are going to show the fundamental steps to compute in order to deform a given object stored in a given file.
# 
# To achieve this, we basically need a parser for extracting from the file all geometrical information (typically the nodes coordinates and the topology), then deforming such nodes with one of the **PyGeM** deformation techniques.
# Here we show a simple `FFD` applied to a `.vtp` and to an `.stl` files. To deal with such files, we employ the [Smithers](https://github.com/mathLab/Smithers) package, a utilities toolbox that allows for an easy manipulation of several file formats.
# 
# As usually, at the beginning we import all the modules we need.

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import numpy as np
import mpl_toolkits.mplot3d
import matplotlib.pyplot as plt
from pygem import FFD
from smithers import io



# For visualization purpose, we also implement a small function that shows the object parsed with **Smithers**.

# In[2]:


def plot(data, color=None):

    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection
    import matplotlib.pyplot as plt

    if color is None:
        color = (0, 0, 1, 0.1)
    fig = plt.figure(figsize=(16,10))

    verts = [data['points'][cell] for cell in data['cells']]
    ax = fig.add_subplot(111, projection='3d')
    faces = Poly3DCollection(verts, linewidths=1, edgecolors='k')
    faces.set_facecolor(color)
    
    ax.add_collection3d(faces)
    ax.set_xlim3d(-10., 10.0)
    ax.set_ylim3d(-10., 10.)
    ax.set_zlim3d(-10., 10.)
    ax.set_aspect('auto','box')

    plt.show()


# ### Deformation of the VTP file
# 
# First of all, we need a `.vtp` file: we download a simple cube and we open it.

# In[3]:


#get_ipython().system('rm -rf cube.vtp')
#get_ipython().system('wget https://raw.githubusercontent.com/mathLab/Smithers/master/tests/test_datasets/cube.vtp')
vtp_filename = "result.vtp"

vtp_content = io.VTPHandler.read(vtp_filename)
plot(vtp_content)


# We can now instantiate a new `FFD` object, setting position and lenght of the lattice of points as well as their displacements. We present here a very simple deformation, moving just one control points, but of course it is possible to change the `FFD` settings according to needed. We also remark that of course any other deformation (by **PyGeM**) can be used!

# In[4]:


ffd = FFD()
ffd.box_length = [20.,20.,20.]
ffd.box_origin = [-10.,-10.,-10.]

ffd.array_mu_x[1, :, :] += 0.2


# Now the actual deformation: the points of our object are morphed through the `ffd` and the new coordinates are replaced to the older one.

# In[5]:


vtp_content['points'] = ffd(vtp_content['points'])


# Here a visual test to see the final outcome!

# In[6]:

new_mesh = vtp_content['points'] 
plot(vtp_content)

ax = plt.figure(figsize=(8, 8)).add_subplot(111, projection='3d')
ax.scatter(*new_mesh.T)
ax.scatter(*ffd.control_points().T, s=50, c='red')
plt.show()

# Of course, the deformed object is still contained within the `vtp_content` dictionary. To save it into a new file, we just need to use again the `VTPHandler` by **Smithers**.

# In[7]:


io.VTPHandler.write('deform_result.vtp', vtp_content)
