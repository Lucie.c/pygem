#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 10 16:03:26 2021

@author: lcalmett
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 11:20:01 2021

@author: lcalmett
"""




import numpy as np
from smithers import io
import matplotlib.pyplot as plt

from pygem import RBF
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import imageio

import sys


def plot(data, color=None):

    

    if color is None:
        color = (0, 0, 1, 0.1)
    fig = plt.figure(figsize=(16,10))

    verts = [data['points'][cell] for cell in data['cells']]
    ax = fig.add_subplot(111, projection='3d')
    faces = Poly3DCollection(verts, linewidths=1, edgecolors='k')
    faces.set_facecolor(color)
    
    ax.add_collection3d(faces)
    ax.set_xlim3d(-10., 20.)
    ax.set_ylim3d(-10., 20.)
    ax.set_zlim3d(-10., 20.)
    ax.set_aspect('auto','box')

    plt.show()
rbf = RBF()


#parameters_file = 'parameters_idw_cube.prm'
filenames = []


#### PREMIERE ROTATION ##############
#idw.read_parameters(filename=parameters_file)
controle_p_ini  = rbf.original_control_points =  np.array([[-10, -10 , -10.        ],
                     [-10 ,-10 , 10.        ],
                     [-10  , 10 ,-10.        ],
                     [-10  , 10  ,10.        ],
                     [ 10  ,-10 ,-10.        ],
                     [ 10  ,-10  ,10.        ],
                     [ 10  ,10  ,-10.        ],
                     [ 10  ,10   ,10.        ]])
test= np.transpose(controle_p_ini) 
teta = 23*np.pi/180
rotation = np.array([[np.cos(teta),-np.sin(teta),0],
                     [np.sin(teta),np.cos(teta),0],
                     [0,0,1]])
test = np.dot(rotation,test)
test= np.transpose(test)

control_p_deformation = controle_p_ini + 0 
for i in range(4,8):
    control_p_deformation[i,0] += 4


rbf.deformed_control_points = test

#l'écrire dans un ficher texte, à la fin pour récupérer les positions?
#b=idw.write_parameters('test')

obj_filename = "init.obj"
obj_content = io.OBJHandler.read(obj_filename)
obj_content['points'] = rbf(obj_content['points'])
io.OBJHandler.write('resultat.obj', obj_content)

new_mesh = obj_content['points'] 



ax = plt.figure(figsize=(8, 8)).add_subplot(111, projection='3d')
ax.scatter(*new_mesh.T)
plt.show()
plot(io.OBJHandler.read('init.obj'))
plot(io.OBJHandler.read('resultat.obj'), (0, 1, 0, .1))  

##### DEUXIEME ROTATION ###########

controle_p_ini  = rbf.original_control_points =test

rotation = np.array([[1,0,0],
                     [0,np.cos(teta),-np.sin(teta)],
                     [0,np.sin(teta),np.cos(teta)]])


test2 = np.dot(rotation,np.transpose(controle_p_ini) )
test2= np.transpose(test2)
rbf.deformed_control_points = test2 


obj_filename = "resultat.obj"
obj_content = io.OBJHandler.read(obj_filename)
obj_content['points'] = rbf(obj_content['points'])
io.OBJHandler.write('resultat2.obj', obj_content)

new_mesh = obj_content['points'] 



ax = plt.figure(figsize=(8, 8)).add_subplot(111, projection='3d')
ax.scatter(*new_mesh.T)
plt.show()
plot(io.OBJHandler.read('resultat.obj'))
plot(io.OBJHandler.read('resultat2.obj'), (0, 1, 0, .1))  
""""""